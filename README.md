
1.端口的修改:
1.1 tomcat的端口
tomcat目录的conf\server.xml 修改:
```
<Connector port="9100" protocol="HTTP/1.1"
               connectionTimeout="20000"
               redirectPort="8443" />
```

1.2 cas服务端的端口:
修改 cas 的 WEB-INF/cas.properties:
server.name=http://localhost:9100

2.去除https的认证(开发的时候不需要https认证)

2.1 修改cas的WEB-INF/deployerConfigContext.xml
```
<bean id="proxyAuthenticationHandler"
          class="org.jasig.cas.authentication.handler.support.HttpBasedServiceCredentialsAuthenticationHandler"
          p:httpClient-ref="httpClient" 
		  p:requireSecure="false"
          />
```

这里增加的是  p:requireSecure="false" , requireSource属性的意思为是否需要安全认证,即HTTPS,false为不需要
默认是true

2.2 修改WEB-INF/spring-configuration/ticketGrantingTicketCookieGenerator.xml
---修改前:
```
<bean id="ticketGrantingTicketCookieGenerator" class="org.jasig.cas.web.support.CookieRetrievingCookieGenerator"
      p:cookieSecure="true"
      p:cookieMaxAge="-1"
      p:cookieName="CASTGC"
      p:cookiePath="/cas" />
```

---修改后:
```
<bean id="ticketGrantingTicketCookieGenerator" class="org.jasig.cas.web.support.CookieRetrievingCookieGenerator"
		p:cookieSecure="false"
		p:cookieMaxAge="3600"
		p:cookieName="CASTGC"
		p:cookiePath="/cas" />
```

参数 p:cookieSecure="true",同理为HTTPS验证相关
参数 P:cookieMaxAge="-1",是COOKIE的最大生命周期,-1位无生命周期,即只在当前打开的窗口有效,关闭或重新打开其它的窗口,仍然会要求验证.
可以根据需要修改为大于0的数字,比如3600,意思是在3600秒内,打开任意的窗口,都不需要验证

2.3 修改cas的 WEB-INF/spring-configuration/warnCookieGenerator.xml

---修改前:
```
<bean id="warnCookieGenerator" class="org.jasig.cas.web.support.CookieRetrievingCookieGenerator"
	p:cookieSecure="true"
	p:cookieMaxAge="-1"
	p:cookieName="CASPRIVACY"
	p:cookiePath="/cas" />
```

---修改后:
```
<bean id="warnCookieGenerator" class="org.jasig.cas.web.support.CookieRetrievingCookieGenerator"
		p:cookieSecure="false"
		p:cookieMaxAge="3600"
		p:cookieName="CASPRIVACY"
		p:cookiePath="/cas" />
```

我们这里将cookieSecure改为false ,  cookieMaxAge 改为3600


3.cas服务端数据源设置

3.1修改cas服务端WEB-INF/deployerConfigContext.xml
在末尾添加下面的配置:

--配置的是数据源 (使用的是C3P0)
```
	<bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource"  
				  p:driverClass="com.mysql.jdbc.Driver"  
				  p:jdbcUrl="jdbc:mysql://127.0.0.1:3306/pinyougoudb?characterEncoding=utf8"  
				  p:user="root"  
				  p:password="root" /> 
```
--配置MD5加密
```
	<bean id="passwordEncoder" class="org.jasig.cas.authentication.handler.DefaultPasswordEncoder"  
		c:encodingAlgorithm="MD5"  
		p:characterEncoding="UTF-8" />  
```
--配置权限控制器   需要引用 dataSource 跟 passwordEncoder  
```
	<bean id="dbAuthHandler"  
		  class="org.jasig.cas.adaptors.jdbc.QueryDatabaseAuthenticationHandler"  
		  p:dataSource-ref="dataSource"  
		  p:sql="select password from tb_user where username = ?"  
		  p:passwordEncoder-ref="passwordEncoder"/>  
```

3.2将权限控制器添加到authenticationManager
```
	<bean id="authenticationManager" class="org.jasig.cas.authentication.PolicyBasedAuthenticationManager">
        <constructor-arg>
            <map>
                <!--
                   | IMPORTANT
                   | Every handler requires a unique name.
                   | If more than one instance of the same handler class is configured, you must explicitly
                   | set its name to something other than its default name (typically the simple class name).
                   -->
                <entry key-ref="proxyAuthenticationHandler" value-ref="proxyPrincipalResolver" />
                <!-- dbAuthHandler是自己添加配置的 -->
                <entry key-ref="dbAuthHandler" value-ref="primaryPrincipalResolver" />
            </map>
        </constructor-arg>
```

 之前的
 ```
	<bean id="primaryAuthenticationHandler"
          class="org.jasig.cas.authentication.AcceptUsersAuthenticationHandler">
        <property name="users">
            <map>
                <entry key="casuser" value="Mellon"/>
                <entry key="admin" value="admin"/>
            </map>
        </property>
    </bean>
```

可以删除

3.3将c3p0-0.9.1.2.jar,car-server-support-jdbc-4,0.0.jar,mysql-coonector-java-5.1.32.jar
添加到webapps\cas\WEB-INF\lib下面

那么到现在为止全部的基本配置就已经完成了
就可以完成从数据库pinyougoudb的tb_user表中查询对用的用户进行认证类的


4.国际化处理错误信息的提示
4.1需要在表单增加:
```
<form:errors path="*" id="msg" cssClass="errors" element="div" htmlEscape="false" />
```
这时候出现用户名不存在或者密码错误的话,提示的错误信息是中文的
4.2配置错误信息的国际化  i18n
4.2.1这个提示信息是在WEB-INF\classes目录下的messages.properties
```
authenticationFailure.AccountNotFoundException=Invalid credentials.
authenticationFailure.FailedLoginException=Invalid credentials.
```
把上面这两段代码复制到 message_zh_CN.properties下,并且修改成中文,
注意不能直接写成中文,而应该写成中文的转义,可以使用eclipse的properties属性文件进行处理
```
authenticationFailure.AccountNotFoundException=\u7528\u6237\u4E0D\u5B58\u5728.
authenticationFailure.FailedLoginException=\u5BC6\u7801\u9519\u8BEF.
```
4.2.2修改cas-servlet.xml文件,将默认的国际化处理改成中文简体的
```
<bean id="localeResolver" class="org.springframework.web.servlet.i18n.CookieLocaleResolver" p:defaultLocale="zh_CN" />
```

第一个是用户不存在时的错误提示
第二个是密码错误的提示




   





